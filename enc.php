<?php 
// namespace Zegl\Hash;



class MD5 
{
    /**
     * MD5::hash()
     *
     * @param $input
     *
     * @return md5($input)
     */
    public function hash($input)
    {
        // Initial variables
        $a0 = 0x67452301;
        $b0 = 0xefcdab89;
        $c0 = 0x98badcfe;
        $d0 = 0x10325476;
        $shifts = [
            7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,
            5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,
            4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,
            6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21
        ];
        $constants = [];
        // Generate constants
        for ($i = 0; $i < 64; $i++) {
            $constants[$i] = floor(abs(sin($i + 1)) * (pow(2, 32))) & 0xffffffff;
        }
        $size = strlen($input) * 8;
        $input = $this->padding($input);
        // Break input into chunks
        $chunks = str_split($input, 64);
        foreach ($chunks as $chunk) {
            // Save the current state of these
            $A = $a0;
            $B = $b0;
            $C = $c0;
            $D = $d0;
            // Split into words and convert each word to little endian
            $words = str_split($chunk, 4);
            foreach ($words as $i => $chrs) {
                $chrs = str_split($chrs);
                $word = '';
                
                // Convert to little endian
                $chrs = array_reverse($chrs);
                foreach ($chrs as $chr) {
                    $word .= sprintf('%08b', ord($chr));
                }
                $words[$i] = bindec($word);
            }
            if (count($words) < 16) {
                 $words[] = 0x00000000ffffffff & $size;
                 $words[] = 0xffffffff00000000 & $size;
            }
            // Main loop
            for ($i = 0; $i < 64; $i++) {
                $step = floor($i /16);
                switch ($step) {
                    case 0;
                        $F = $this->F($B, $C, $D);
                        $g = $i;
                        break;
                    case 1:
                        $F = $this->G($B, $C, $D);
                        $g = (5 * $i + 1) % 16;
                        break;
                    case 2:
                        $F = $this->H($B, $C, $D);
                        $g = (3 * $i + 5) % 16;
                        break;
                    case 3:
                        $F = $this->I($B, $C, $D);
                        $g = (7 * $i) % 16;
                        break;
                }
                $dTemp = $D;
                $D = $C;
                $C = $B;
                $B = $B + $this->leftrotate(($A + $F + $constants[$i] + $words[$g]) & 0xffffffff, $shifts[$i]);
                $A = $dTemp;
            }
            // Add hash of chunk to result
            $a0 = $A + $a0 & 0xffffffff;
            $b0 = $B + $b0 & 0xffffffff;
            $c0 = $C + $c0 & 0xffffffff;
            $d0 = $D + $d0 & 0xffffffff;
        }
        return bin2hex(pack('V4', $a0, $b0, $c0, $d0));
    }
    private function F($b, $c, $d)
    {
        return ($b & $c) | (~$b & $d);
    }
    private function G($b, $c, $d)
    {
        return ($d & $b) | (~$d & $c);
    }
    private function H($b, $c, $d)
    {
        return $b ^ $c ^ $d;
    }
    private function I($b, $c, $d)
    {
        return $c ^ ($b | ~$d);
    }
    /**
     * MD5::padding()
     *
     * Append "0" bit until message length in bits ≡ 448 (mod 512)
     *
     * @param $input
     */
    private function padding($input)
    {
        // Add a single "1"
        $input .= chr(128);
        // Pad with "0"
        while (((strlen($input) + 8) % 64) !== 0) {
            $input .= chr(0);
        }
        return $input;
    }
    /**
     * MD5::leftrotate()
     *
     */
    private function leftrotate($x, $c)
    {
        return ($x << $c) | ($x >> (32 - $c));
    }
    public function test($test)
    {
        return md5($test);
    }
}

?>